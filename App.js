import React from 'react'
import {
} from 'react-native'

import SwitchNavigator from './src/navigators/SwitchNavigator'

import { AuthProvider } from './src/context/AuthContext'

import { NavigationContainer } from '@react-navigation/native'

const App = ()  => {
  return (
    <NavigationContainer>
      <AuthProvider>
        <SwitchNavigator />
      </AuthProvider>
    </NavigationContainer>
  )
}

export default App
