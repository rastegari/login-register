import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import Login from '../screens/Login'
import Register from '../screens/Register'
import Index from '../screens/Index'

import { useAuthState } from '../context/AuthContext'

const Stack = createStackNavigator()

export default SwtichNavigator = () => {
  const authState = useAuthState()

  return (
    <Stack.Navigator
      screenOptions={{
        title: 'Login Register',
        headerStyle: {
          backgroundColor: '#9c27b0',
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      {
        authState.isLoggedin ?
          <>
            <Stack.Screen
              name="Index"
              component={Index}
            />
          </>
          :
          <>
            <Stack.Screen
              name='Login'
              component={Login}
            />
            <Stack.Screen
              name='Register'
              component={Register}
            />
          </>
      }
    </Stack.Navigator>
  )
}