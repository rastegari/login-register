import React, {
  createContext,
  useReducer,
  useContext,
  useEffect
} from 'react'
import AsyncStorage from '@react-native-community/async-storage'

const AuthStateContext = createContext()
const AuthDispatchContext = createContext()

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
    case "LOGOUT":
      return {
        ...state,
        isLoggedin: !state.isLoggedin
      }
    default:
      return {
        ...state
      }
  }
}

const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, {
    isLoggedin: false
  })

  useEffect(() => {
    const loader = async () => {
      const token = await AsyncStorage.getItem('@token')

      if (token !== null) {
        dispatch({ type: 'LOGIN' })
      }
    }

    loader()
  }, [])

  return (
    <AuthStateContext.Provider value={state}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  )
}

const useAuthState = () => {
  const context = useContext(AuthStateContext)

  if (context === undefined) {
    throw new Error('useAuthState must be used within a AuthProvider')
  }

  return context
}

const useAuthDispatch = () => {
  const context = useContext(AuthDispatchContext)

  if (context === undefined) {
    throw new Error('useAuthDispatch must be used within a AuthProvider')
  }
  
  return context
}


export { AuthProvider, useAuthState, useAuthDispatch }