import React from 'react'
import {
  View,
  Text,
  StyleSheet
} from 'react-native'

import Button from '../components/layout/Button'

import { useAuthDispatch } from '../context/AuthContext'
import AsyncStorage from '@react-native-community/async-storage'

const Index = () => {
  const authDispatch = useAuthDispatch()

  const logoutHandler = () => {
    AsyncStorage.removeItem('@token')
    authDispatch({ type: 'LOGOUT' })  
  }

  return (
    <View style={styles.container}>
      <Text>Index</Text>
      <Button
        value="Logout"
        color="#9c27b0"
        size={100}
        handler={logoutHandler}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'    
  }
})

export default Index
