import React, { useState } from 'react'
import {
  View,
  Text,
  StyleSheet,
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import TextInputGroup from '../components/layout/TextInputGroup'
import Button from '../components/layout/Button'

import { useAuthDispatch } from '../context/AuthContext'

import { BASE_URL } from '../../config'

const Login = ({ navigation }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [loginError, setLoginError] = useState('')

  const authDispatch = useAuthDispatch()

  const loginHandler = () => {
    try {
      const user = {
        username,
        password
      }

      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
      }

      fetch(`${BASE_URL}/accounts/login/`, options)
        .then(async response => {
          if (response.status === 200){
            const data = await response.json()
            AsyncStorage.setItem('@token', data.token)
            authDispatch({ type: 'LOGIN' })
          } else {
            setLoginError("Invalid credentials!")
          }
        })
    } catch (error) {
      console.error(error)
    }
  }

  const registerNavigationHandler = () => {
    navigation.navigate("Register")
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Login</Text>

      <TextInputGroup
        label='Username'
        onChange={setUsername}
      />
      <TextInputGroup
        label='Password'
        onChange={setPassword}
        secure={true}
      />

      <Text style={styles.error}>{loginError}</Text>

      <Button
        value="Login"
        color="#9c27b0"
        size={100}
        handler={loginHandler}
      />
      <Button
        value="Register"
        color="#9c27b0"
        size={100}
        handler={registerNavigationHandler}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'    
  },
  text: {
    fontSize: 18,
    color: '#9c27b0',
    fontWeight: 'bold'
  },
  error: {
    color: 'red'
  }
})

export default Login
