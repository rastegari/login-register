import React, { useState } from 'react'
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import TextInputGroup from '../components/layout/TextInputGroup'
import Button from '../components/layout/Button'

import { useAuthDispatch } from '../context/AuthContext'

const Register = () => {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const authDispatch = useAuthDispatch()

  const registerHandler = () => {
    try {
      AsyncStorage.setItem('@token', 'ABC')
      authDispatch({ type: 'LOGIN' })
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.text}>Register</Text>

        <TextInputGroup
          label='First Name'
          onChange={setFirstName}
        />
        <TextInputGroup
          label='Last Name'
          onChange={setLastName}
        />
        <TextInputGroup
          label='Email'
          onChange={setEmail}
        />
        <TextInputGroup
          label='Username'
          onChange={setUsername}
        />
        <TextInputGroup
          label='Password'
          onChange={setPassword}
          secure={true}
        />
        <TextInputGroup
          label='Confirm Password'
          onChange={setConfirmPassword}
          secure={true}
        />

        <Button value="Register"
          color="#9c27b0"
          size={100}
          handler={registerHandler}
        />
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30 
  },
  text: {
    fontSize: 18,
    color: '#9c27b0',
    fontWeight: 'bold'
  }
})

export default Register
